﻿using System;

namespace CSharpTutorial
{
    class Program
    {
        static void Main(string[] args)
        {
            #region Part 1 - Introduction
            // Introcution C#
            #endregion

            #region Part 2 - How to write your first program (Hello World!)
            //Console.WriteLine("Please enter your name:");
            //var name = Console.ReadLine();

            //Console.WriteLine($"Hello {name}!");
            #endregion

            #region Part 3 - Comments
            /* var city = Console.ReadLine();
             //if city is delhi
             if (city == "Delhi")
             {
                 //some code
             }*/

            //SomeClass someClass = new SomeClass();
            //someClass.SomeProperty = 1;
            //someClass.SomeMethod();

            //Math math = new Math();
            //var result = math.Divide(15, 3);
            #endregion

            #region Part 4 - Data types
            //Console.WriteLine("sbyte {0} to {1}", sbyte.MinValue, sbyte.MaxValue);
            //Console.WriteLine("byte {0} to {1}", byte.MinValue, byte.MaxValue);

            //Console.WriteLine("short {0} to {1}", short.MinValue, short.MaxValue);
            //Console.WriteLine("ushort {0} to {1}", ushort.MinValue, ushort.MaxValue);

            //Console.WriteLine("int {0} to {1}", int.MinValue, int.MaxValue);
            //Console.WriteLine("uint {0} to {1}", uint.MinValue, uint.MaxValue);

            //Console.WriteLine("long {0} to {1}", long.MinValue, long.MaxValue);
            //Console.WriteLine("ulong{0} to {1}", ulong.MinValue, ulong.MaxValue);

            //sbyte sb = 128; //Incorrect
            //sbyte sb = 127; //Correct

            //char chr = 'c';
            //float fnum = 10.5F;
            //double dnum = 15.4D;
            //decimal dcnum = 10.8M;

            //float fnum = 100F / 3;
            //double dnum = 100D / 3;
            //decimal dcnum = 100M / 3;
            //Console.WriteLine("float {0}", fnum);
            //Console.WriteLine("double {0}", dnum);
            //Console.WriteLine("decimal {0}", dcnum);

            //bool b = true;

            //int i = default(int); // 0
            //bool b = default(bool); //false

            //bool? AreYouStudent = null;
            #endregion

            #region Part 5 - Value types and reference types
            //int i = 20;
            //int j = i;
            //Console.WriteLine("i={0}", i);
            //Console.WriteLine("j={0}", j);
            //j = 50;
            //Console.WriteLine("i={0}", i);
            //Console.WriteLine("j={0}", j);

            //House house1, house2;
            //house1 = new House();
            //house1.Area = 1200;
            //house2 = house1;
            //Console.WriteLine("Area of house 1={0}", house1.Area);
            //Console.WriteLine("Area of house 2={0}", house2.Area);

            //house2.Area = 1000;
            //Console.WriteLine("Area of house 1={0}", house1.Area);
            //Console.WriteLine("Area of house 2={0}", house2.Area);

            //int x = 1;
            //Increment(ref x);
            //Console.WriteLine("The value of x is: " + x);

            //int i = 5;
            //Object obj = i; //Boxing
            //i = (int)obj; //Unboxing

            //string s = "this is c# tutorial";
            //s = "test";
            #endregion

            #region Part 6 - Variables & Operators
            //int i = 10, j = 20;
            //int i = 10;
            //bool j = false;

            //int i;
            //int j = i * 15;

            //VarTest varTest = new VarTest();
            //Console.WriteLine(varTest.i);

            //var i = 10;
            //var b = false;

            //var a = false;
            //Type aType = a.GetType();
            //Console.WriteLine("a is type of: " + aType.ToString());

            //VarTest varTest = new VarTest();
            //varTest.i = 10;

            //var a = false;
            //if (a)
            //{
            //    int i = 0;

            //}
            //i= 10;

            //for (int i = 0; i < 10; i++)
            //{
            //    Console.WriteLine(i);
            //}


            //int j = 10;
            //for (int i = 0; i < 10; i++)
            //{
            //    int j = 20; // This is invalid as variable j is still in scope.  
            //};

            //int i = 10;
            //if (i is object)
            //{
            //    Console.WriteLine("i is an object");
            //}

            //object obj = "testing as operator";
            //string s = obj as string;

            //Console.WriteLine(sizeof(int));

            //Console.WriteLine(typeof(VarTest));

            //int? a = null;
            //int b = a ?? 10;


            //var input = Console.ReadLine();
            //if (input == "")
            //{
            //    Console.WriteLine("input string is empty");
            //}
            //else if (input.Length < 5)
            //{
            //    Console.WriteLine("input string has less than 5 characters");
            //}
            //else if (input.Length < 10)
            //{
            //    Console.WriteLine("input string has less than 10 characters");
            //}


            //var i = 0;
            //if (i == 0)
            //{
            //    Console.WriteLine("i is zero");
            //}
            //else
            //{
            //    Console.WriteLine("i is non-zero");
            //}


            //var output = i == 0 ? "i is zero" : "i is non-zero";
            //Console.WriteLine(output);
            #endregion

            #region Part 7 - Control flow - if, else and switch statements
            //var country = Console.ReadLine();
            //switch (country)
            //{
            //    case "America":

            //        goto case "Britain";

            //    case "France":
            //     Console.WriteLine("Language is French");
            //        break;

            //    case "Britain":
            //     Console.WriteLine("Language is English");
            //        break;

            //    default:
            //        Console.WriteLine("Language is not defined");
            //        break;
            //}
            #endregion

            Console.ReadLine();

        }

        #region Part 5 - Value types and reference types
        //static void Increment(ref int i)
        //{
        //    i = i + 1;
        //}
        #endregion
    }

    #region Part 7 - Control flow - if, else and switch statements
    //public class ScopeTest
    //{
    //    int i = 10;

    //    public void ScopeMethod()
    //    {
    //        int i = 20; // Valid and hides the variable at type level.
    //    }
    //}
    //public class VarTest
    //{
    //    public int i;
    //}
    #endregion

    #region Part 3 - Comments
    ///// <summary>
    ///// this represent an example class.
    ///// </summary>
    //public class SomeClass
    //{
    //    /// <summary>
    //    /// This represent some property.
    //    /// </summary>
    //    public int SomeProperty { get; set; }


    //    /// <summary>
    //    /// This represent some method.
    //    /// </summary>
    //    public void SomeMethod()
    //    {
    //       //some code
    //    }

    //    /// <summary>
    //    /// This is another method
    //    /// </summary>
    //    /// <param name="i">first number</param>
    //    /// <param name="j">second number</param>
    //    /// <returns>a number</returns>
    //    public int AnotherMethod(int i, int j)
    //    {
    //        return 1;
    //    }
    //}

    ///// <summary>
    ///// The class represent Math for airthmatic calculations
    ///// </summary>
    //public class Math
    //{
    //    /// <summary>
    //    /// This method is used to divide a double value by another double value.
    //    /// </summary>
    //    /// <remarks>This is further information about this method.</remarks>
    //    /// <exception cref="DivideByZeroException"></exception>
    //    /// <seealso cref="Multiply(double, double)"/>
    //    /// <example>
    //    /// <code>
    //    ///     Math math = new Math();
    //    ///     var result = math.Divide(12, 4);
    //    /// </code>
    //    /// </example>
    //    /// <param name="i">The number that is being divided i.e. dividend</param>
    //    /// <param name="j">The number that is being divided by i.e. divisor</param>
    //    /// <returns>The quotient of division.</returns>
    //    public double Divide(double i, double j)
    //    {
    //        if (j == 0)
    //            throw new DivideByZeroException();
    //        return i / j;
    //    }

    //    /// <summary>
    //    /// This method is used to multiply two double values
    //    /// </summary>
    //    /// <param name="i">The first number</param>
    //    /// <param name="j">The second number</param>
    //    /// <returns>The product of two numbers.</returns>
    //    public double Multiply(double i, double j)
    //    {
    //        return i * j;
    //    }
    //}
    #endregion

}
